const $categoriaName = document.querySelector('.nameCategoria') 
const $seccionName = document.querySelector('.seccion')
const $categorias = document.querySelector('.categorias1')
const $template = document.querySelector('#template') 
const $fragment = document.createDocumentFragment();

const categorias = {}
const produc1 = { nombre: 'arroz', catidad: 5, precio: 1500 } 
const produc2 = {nombre: 'sal', catidad: 8, precio: 1000 } 
const produc3 = { nombre: 'fideo', catidad: 4, precio: 1800 }

const categoria = (id, categoriaNombre, seccion) => {
    const catego = {
        id: id,
        categoriaNombre: categoriaNombre,
        seccion: seccion,
        productos: []
    }

    const existeId = categorias.hasOwnProperty(catego.categoriaNombre)
    
    existeId
    ? console.log('la categoria existe')
    : categorias[catego.categoriaNombre] = catego
    
    
}


const agregarProducto = (categoria, desProducto) => {
    const existeId = categorias.hasOwnProperty(categoria)

    existeId
    ? categorias[categoria].productos.push(desProducto) 
    : console.log(`No se encontro ${categoria}`)
    
}

categoria(1, 'VERDURAS', 3)
categoria(1, 'VERDURAS', 5)
categoria(2, 'BEBIDAS', 8)
categoria(2, 'BEBIDAS', 2)
categoria(3, 'MEKATOS', 6)
categoria(3, 'MEKATOS', 4)
categoria(4, 'CARNICOS', 3)
categoria(5, 'LACTEOS', 5)
categoria(6, 'PANADERIA', 3)

agregarProducto('VERDURAS', produc1)
agregarProducto('VERDURAS', produc2)
agregarProducto('MEKATOS', produc3)



const pintarCategorias = () => {
    
    Object.keys(categorias).map(clave => {
        const valor = categorias[clave].categoriaNombre
        const seccion = categorias[clave].seccion
        const clone = $template.content.cloneNode(true)
        clone.querySelector('h5').textContent = valor
        clone.querySelector('span').textContent = `seccion ${seccion}`   
        $fragment.appendChild(clone)       
    
        console.log(valor)
    })
    $categorias.appendChild($fragment)
}


pintarCategorias()
